import React, { createContext, useState, useEffect } from 'react';

const CartContext = createContext();

export function CartProvider({ children }) {
  const [cartItems, setCartItems] = useState([]);

  const addToCart = (product) => {
    const existingProduct = cartItems.find((item) => item.id === product.id);

    if (existingProduct) {
      // Product already exists in the cart, update the quantity
      const updatedCart = cartItems.map((item) =>
        item.id === product.id ? { ...item, quantity: item.quantity + 1 } : item
      );
      setCartItems(updatedCart);
    } else {
      // Product doesn't exist in the cart, add it as a new item
      const updatedCart = [...cartItems, { ...product, quantity: 1 }];
      setCartItems(updatedCart);
    }
  };

  const removeFromCart = (productId) => {
    const updatedCart = cartItems.filter((item) => item.id !== productId);
    setCartItems(updatedCart);
  };

  const clearCart = () => {
    setCartItems([]);
  };

  const getTotalItems = () => {
    let totalItems = 0;
    cartItems.forEach((item) => {
      totalItems += item.quantity;
    });
    return totalItems;
  };

  const updateCart = (updatedCartItems) => {
    setCartItems(updatedCartItems);
  };

  useEffect(() => {
    // You can save the cartItems to the database whenever it changes
    saveCartToDatabase();
  }, [cartItems]);

  const saveCartToDatabase = () => {
    // Assuming you are using fetch or axios for API requests
    fetch('https://example.com/save-cart', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(cartItems),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log('Cart items saved to the database:', data);
      })
      .catch((error) => {
        console.error('Error saving cart items to the database:', error);
      });
  };

  return (
    <CartContext.Provider
      value={{
        cartItems,
        addToCart,
        removeFromCart,
        clearCart,
        getTotalItems,
        updateCart,
      }}
    >
      {children}
    </CartContext.Provider>
  );
}

export default CartContext;
