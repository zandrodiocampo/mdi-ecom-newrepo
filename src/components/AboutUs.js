import './AboutUs.css';

export default function AboutUs () {
    return (
        <>
        <div className='aboutUs-container'>
            <h3>Welcome to Pawsome Pet Products!</h3>
            <p>At Pawsome Pet Products, we understand that your furry friends are an integral part of your family. That's why we're dedicated to providing the highest quality pet products that cater to their unique needs. We are a leading online destination for pet lovers, offering a wide range of carefully curated items to ensure that your pets live their happiest and healthiest lives.

Our Mission:
Our mission is to enhance the well-being of pets and their owners by offering top-notch products, exceptional customer service, and expert advice. We believe that every pet deserves the best care, comfort, and enjoyment, and we strive to make that a reality by delivering exceptional pet products right to your doorstep.

Quality and Selection:
We take pride in sourcing the finest pet products available on the market. Our team carefully selects each item, ensuring that it meets our rigorous quality standards. Whether it's nutritious pet food, cozy bedding, interactive toys, stylish apparel, or essential grooming supplies, we have everything you need to pamper your beloved pets.

Expert Advice:
At Pawsome Pet Products, we're more than just an online store. We're also your trusted resource for pet care information and advice. Our knowledgeable team of pet enthusiasts is always here to assist you in making informed decisions about your pet's well-being. Whether you have questions about nutrition, training, or general pet care, we're here to provide reliable guidance and support.

Customer Satisfaction:
We prioritize your satisfaction above everything else. From the moment you enter our website to the delivery of your order, we aim to provide you with a seamless and enjoyable shopping experience. We offer secure payment options, fast shipping, and hassle-free returns to ensure that you're completely satisfied with your purchase. Your trust and loyalty mean the world to us, and we continually strive to exceed your expectations.

Community and Giving Back:
We believe in giving back to the pet community that has given us so much joy. That's why we actively support animal welfare organizations and initiatives that improve the lives of pets in need. With every purchase you make, you're helping us make a positive difference in the lives of animals around the world.

Join the Pawsome Family:
We invite you to join our ever-growing community of pet lovers. Explore our vast collection of pet products, find inspiration, and share your experiences with fellow pet owners. Sign up for our newsletter to stay updated on the latest trends, promotions, and exclusive offers.

Thank you for choosing Pawsome Pet Products as your go-to destination for all things pet-related. We're committed to serving you and your furry companions with love, care, and unmatched dedication. Together, let's create a world where every pet is happy, healthy, and cherished.</p>
        </div>
        </>
    )
}