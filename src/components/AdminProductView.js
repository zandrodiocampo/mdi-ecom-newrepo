import {useContext} from 'react';
import {Card} 		  from 'react-bootstrap';
import {Link}                 from 'react-router-dom';
import UserContext            from '../UserContext';
import "../App.css";

export default function AdminProductView({ProductProp}){
	const {user}                          = useContext(UserContext);
	const {name, description, price, _id} = ProductProp
	
	return (
		<>
		<Card className=" ProductCard">
			<Card.Body>
			<Card.Title>{name}</Card.Title>
			<Card.Text>{description}</Card.Text>
			<Card.Subtitle>Price</Card.Subtitle>
			<Card.Text>{price}</Card.Text>
				{  
					(user._id !== null)?
					<>
					<Link className="btn btn-primary" to={`/ProductView/${_id}`}>View Details</Link>
					</>
					:
					<>
					<Link className="btn btn-primary" to={`/login`}>Login</Link>
					</>
				}
			</Card.Body>
		</Card>
		</>
	)


}


