import { useContext} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';
import UserContext from '../UserContext';
import './AppNavBar.css';

export default function AppNavbar(){

	const {user} = useContext(UserContext);

	// state hook to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	console.log(user);

	return (
	
	<Navbar expand="lg" className="sticky-top">
	  <Container  id="navbar" className="w-100">
	    <Navbar.Brand as={Link} to="/" id="navBrand">GroupTwo Petshop</Navbar.Brand>
	     <Navbar.Toggle aria-controls="basic-navbar-nav" />
	       <Navbar.Collapse id="basic-navbar-nav">
	        <Nav className="me-auto navigationBar">         
	           <Nav.Link as={Link} to="/Product">Products</Nav.Link>
			   


				{	
					(user.id !== null) ?
					<>
						<Nav.Link as={Link} to="/getAllProducts">Admin Portal</Nav.Link>
						<Nav.Link as={Link} to="/profile">Profile</Nav.Link>
						<Nav.Link as={Link} to="/logout">Logout</Nav.Link> 
						

					</>      
					:
					<>
						<Nav.Link as={Link} to="/login">Login</Nav.Link>
		           		<Nav.Link as={Link} to="/register">Register</Nav.Link>
		           	</>

		     	}
			  <Nav.Link as={Link} to="/Cart">Cart</Nav.Link> 
	         </Nav>
	       </Navbar.Collapse>
	   </Container>
	</Navbar>

	)	
};

