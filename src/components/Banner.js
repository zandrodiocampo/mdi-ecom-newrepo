import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
export default function Banner(){
	return(
	<div id="banner">	
		<Row>
			<Col className = "p-5">
				<h1 className="bannerText">Max & Sky Pet Supplies</h1>
				
			</Col>
		</Row>
	</div>
	)
}