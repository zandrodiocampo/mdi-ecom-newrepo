import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import CartContext from '../CartContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const { addToCart } = useContext(CartContext);
  const history = useNavigate();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const { ProductId } = useParams();

  const enroll = () => {
    fetch(`https://mdi-ecom-backend.onrender.com/orders/addOrder`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        ProductId: ProductId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Added to cart!',
            icon: 'success',
          });

          addToCart({ id: ProductId, name, description, price });
          history('/Product');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again later',
          });
        }
      });
  };

  useEffect(() => {
    fetch(`https://mdi-ecom-backend.onrender.com/products/getSingleProduct/${ProductId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });

  }, [ProductId]);
  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 4 }}>
          <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src="https://media.istockphoto.com/photos/toys-for-dogs-isolated-picture-id1316829099?b=1&k=20&m=1316829099&s=170667a&w=0&h=atLJanAvHDI8xCKTbMPP6STDHKkFQM3oAqyzarSXOdg=" />
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price {price}</Card.Subtitle>
              {user.id !== null ? (
                <Button variant="primary" onClick={enroll}>Add to Cart</Button>
              ) : (
                <Link className="btn btn-danger" to="/login">Log In</Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
