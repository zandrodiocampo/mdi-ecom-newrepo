import React, { useState, useEffect } from 'react';

const Sort = ({ setProducts }) => {
  const [sortType, setSortType] = useState('');

  useEffect(() => {
    if (sortType) {
      fetchProducts();
    }
  }, [sortType]);

  const fetchProducts = async () => {
    try {
      let url = '';

      if (sortType === 'name_asc') {
        url = 'http://localhost:3000/Product/a-z';
      } else if (sortType === 'price_asc') {
        url = 'http://localhost:3000/Product/low-high';
      }

      const response = await fetch(url);
      if (!response.ok) {
        alert("error fetching products")
      }
      const products = await response.json();

      setProducts(products);
    } catch (error) {
      alert("error fetching prodcuts")
    }
  };

  const handleSortChange = (e) => {
    const value = e.target.value;
    setSortType(value);
  };

  return (
    <div>
      <label htmlFor="sort-select">Sort by:</label>
      <select id="sort-select" value={sortType} onChange={handleSortChange}>
        <option value="">-- Sort --</option>
        <option value="name_asc">Name: A to Z</option>
        <option value="price_asc">Price: Lowest to Highest</option>
      </select>
    </div>
  );
};

export default Sort;
