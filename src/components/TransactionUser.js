import React from 'react';

const TransactionUser = () => {
  const handleViewPDF = () => {
    fetch('/api/transaction-history.pdf')
      .then(response => response.blob())
      .then(blob => {
        const url = URL.createObjectURL(blob);
        window.open(url, '_blank');
      })
      .catch(error => {
        console.error('Error occurred while viewing the PDF:', error);
      });
  };

  return (
    <button onClick={handleViewPDF}>
      View Transaction History PDF
    </button>
  );
};

export default TransactionUser;
