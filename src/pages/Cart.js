import React, { useContext, useState } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import CartContext from '../CartContext';
import PayCard from './PayCard';

export default function Cart() {
  const { cartItems, removeFromCart, updateCart } = useContext(CartContext);
  const [onCheckout, setOnCheckout] = useState([]);

  const handleToggleCheckbox = (itemId) => {
    if (onCheckout.includes(itemId)) {
      setOnCheckout(onCheckout.filter((id) => id !== itemId));
    } else {
      setOnCheckout([...onCheckout, itemId]);
    }
  };

  const handleIncreaseQuantity = (itemId) => {
    const updatedCartItems = cartItems.map((item) => {
      if (item.id === itemId) {
        return {
          ...item,
          quantity: item.quantity + 1,
        };
      }
      return item;
    });
    updateCart(updatedCartItems);
  };

  const handleDecreaseQuantity = (itemId) => {
    const updatedCartItems = cartItems.map((item) => {
      if (item.id === itemId && item.quantity > 0) {
        return {
          ...item,
          quantity: item.quantity - 1,
        };
      }
      return item;
    });
    updateCart(updatedCartItems);
  };

  const handleCheckout = () => {
    const selectedCartItems = cartItems.filter((item) =>
      onCheckout.includes(item.id)
    );
    console.log(selectedCartItems)
    // Send the selected cart items to the backend for checkout
    fetch('/api/checkout', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(selectedCartItems),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return response.json();
      })
      .then((data) => {
        console.log('Checkout successful:', data);
      })
      .catch((error) => {
        console.error('Error during checkout:', error);
      });
  };

  const totalPrice = cartItems.reduce((total, item) => {
    if (onCheckout.includes(item.id)) {
      return total + item.price * item.quantity;
    }
    return total;
  }, 0);

  return (
    <Container className="mt-5">
      <Row>
        <Col>
          {cartItems.length === 0 ? (
            <h3>Your cart is empty.</h3>
          ) : (
            <>
              {cartItems.map((item) => (
                <Card key={item.id} className="mb-3">
                  <Card.Body>
                    <div>
                      <label htmlFor="checkbox">Select to checkout</label>
                      <input
                        type="checkbox"
                        id="checkbox"
                        checked={onCheckout.includes(item.id)}
                        onChange={() => handleToggleCheckbox(item.id)}
                      />
                    </div>
                    <Card.Title>{item.name}</Card.Title>
                    <Card.Text>{item.description}</Card.Text>
                    <p>Quantity: {item.quantity}</p>
                    <Card.Subtitle>Price: {item.price}</Card.Subtitle>
                    <div>
                      <Button
                        variant="secondary"
                        onClick={() => handleDecreaseQuantity(item.id)}
                      >
                        -
                      </Button>
                      <Button
                        variant="secondary"
                        onClick={() => handleIncreaseQuantity(item.id)}
                      >
                        +
                      </Button>
                    </div>
                    <Button
                      variant="danger"
                      onClick={() => removeFromCart(item.id)}
                    >
                      Remove
                    </Button>
                  </Card.Body>
                </Card>
              ))}
              <p>Total Price: ₱{totalPrice}</p>
              <div>
                <h3>Setup payment</h3>
                <PayCard/>
              </div>
              <Button variant="primary" onClick={handleCheckout}>
                Checkout
              </Button>
            </>
          )}
        </Col>
      </Row>
    </Container>
  );
}
