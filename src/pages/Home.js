
import './Home.css';
import AboutUs from '../components/AboutUs';
import Footer from '../components/Footer';
import Highlights from '../components/Highlights';

export default function Home(){

	return(
		<>	
		<div id="jumbotron"></div>
		<Highlights/>
		<AboutUs/>
		<Footer/>
		</>
		
	)
}

