import React, { useState } from "react";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBInput,
  MDBRow,
} from "mdb-react-ui-kit";

export default function PayCard() {
  const [cardNumber, setCardNumber] = useState("");
  const [cardholderName, setCardholderName] = useState("");
  const [expiration, setExpiration] = useState("");
  const [cvv, setCVV] = useState("");
  const [cardDetailsSaved, setCardDetailsSaved] = useState(false);

  const handleCardSubmit = () => {
    // Validate card number
    const isCardNumberValid = /^\d{16}$/.test(cardNumber);
    if (!isCardNumberValid) {
      alert("Please enter a card number.");
      return;
    }
    const isExpiryValid = /^(0[1-9]|1[0-2])\/\d{4}$/.test(expiration);
    if (!isExpiryValid) {
      alert("enter valid MM/YYYY");
      return;
    }

    // Save card details to local storage
    const cardDetails = {
      cardNumber,
      cardholderName,
      expiration,
      cvv,
    };
    localStorage.setItem("cardDetails", JSON.stringify(cardDetails));

    // Update the state to indicate that card details are saved
    setCardDetailsSaved(true);
  };

  const retrieveCardDetails = () => {
    // Retrieve card details from local storage
    const storedCardDetails = localStorage.getItem("cardDetails");
    if (storedCardDetails) {
      const parsedCardDetails = JSON.parse(storedCardDetails);
      setCardNumber(parsedCardDetails.cardNumber);
      setCardholderName(parsedCardDetails.cardholderName);
      setExpiration(parsedCardDetails.expiration);
      setCVV(parsedCardDetails.cvv);
      setCardDetailsSaved(true);
    }
  };

  // Check if card details are available in local storage on component mount
  React.useEffect(() => {
    retrieveCardDetails();
  }, []);

  return (
    <MDBContainer fluid className="py-5 gradient-custom">
      <MDBRow className="d-flex justify-content-center py-5">
        <MDBCol md="7" lg="5" xl="4">
          <MDBCard style={{ borderRadius: "15px" }}>
            <MDBCardBody className="p-4">
              <MDBRow className="d-flex align-items-center">
                <MDBCol size="9">
                  <MDBInput
                    label="Card Number"
                    id="form1"
                    type="number"
                    value={cardNumber}
                    onChange={(e) => setCardNumber(e.target.value)}
                  />
                </MDBCol>
                <MDBCol size="3">
                  <img
                    src="https://img.icons8.com/color/48/000000/visa.png"
                    alt="visa"
                    width="64px"
                  />
                </MDBCol>

                <MDBCol size="9">
                  <MDBInput
                    label="Cardholder's Name"
                    id="form2"
                    type="text"
                    value={cardholderName}
                    onChange={(e) => setCardholderName(e.target.value)}
                  />
                </MDBCol>

                <MDBCol size="6">
                  <MDBInput
                    label="Expiration"
                    id="form2"
                    type="text"
                    placeholder="MM/YYYY"
                    value={expiration}
                    onChange={(e) => setExpiration(e.target.value)}
                  />
                </MDBCol>
                <MDBCol size="3">
                  <MDBInput
                    label="CVV"
                    id="form2"
                    type="password"
                    value={cvv}
                    onChange={(e) => setCVV(e.target.value)}
                  />
                </MDBCol>
                <MDBCol size="3">
                  <MDBBtn
                    color="info"
                    rounded
                    size="lg"
                    onClick={handleCardSubmit}
                  >
                    Save
                    <MDBIcon fas icon="arrow-right" />
                  </MDBBtn>
                </MDBCol>
              </MDBRow>
              {cardDetailsSaved && (
                <div>
                  <h5>Card Details:</h5>
                  <p>Card Number: {cardNumber}</p>
                  <p>Cardholder's Name: {cardholderName}</p>
                </div>
              )}
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}
