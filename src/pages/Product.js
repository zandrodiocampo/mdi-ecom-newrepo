import AdminProductView from "../components/AdminProductView";
import {useState, useEffect} from "react";
import Sort from "../components/Sort";
import Search from "../components/Search";
import "./Product.css";

export default function Product(){

	const [product, setProduct] = useState([]);

	useEffect(() => {
		fetch("https://mdi-ecom-backend.onrender.com/products/ActiveProducts")
		.then(res => res.json())
		.then(data => {
			console.log(data);

		setProduct(data.map(product => {
			return (
				<div className="products-container">
				<AdminProductView key={product._id} ProductProp = {product}/>
				</div>
			)
		}));
		});

	}, []);

	return(

	<>
		<h1>Available Products</h1>
		<Sort/>
		<Search/>
		<div className="products-display">
		{product}
		</div>
	</>
	)

}