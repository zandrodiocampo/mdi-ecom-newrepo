import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import CartContext from '../CartContext';
import UserContext from '../UserContext';
import TransactionUser from '../components/TransactionUser';

export default function Profile() {
  const { user } = useContext(UserContext);
  const { getTotalItems } = useContext(CartContext);

  const history = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [cardDetails, setCardDetails] = useState({});

  const { userId } = useParams();

  useEffect(() => {
    fetch(`https://mdi-ecom-backend.onrender.com/users/details/${userId}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
      });
  }, [userId]);

  useEffect(() => {
    const storedCardDetails = localStorage.getItem('cardDetails');
    if (storedCardDetails) {
      setCardDetails(JSON.parse(storedCardDetails));
    }
  }, []);

  return (
    <>
    <Container className="mt-5">
      <Row>
        <Col>
          <Card className="p-2">
            <Card.Body>
              <h3 className="pb-3">Profile:</h3>
              <Card.Title className="mb-3">
                Name: {lastName}, {firstName}
              </Card.Title>
              <Card.Subtitle className="mb-3">{email}</Card.Subtitle>
              <h5>Items in cart: {getTotalItems()}</h5>
              <TransactionUser/>
            </Card.Body>
          </Card>
        </Col>
      </Row>
	  {Object.keys(cardDetails).length > 0 && (
                <Card className="mt-3">
                  <Card.Body>
                    <Card.Title>Card Details</Card.Title>
                    <Card.Text>
                      Card Number: {cardDetails.cardNumber}
                    </Card.Text>
                    <Card.Text>
                      Cardholder's Name: {cardDetails.cardholderName}
                    </Card.Text>
                  </Card.Body>
                </Card>
              )}
      <Link className="btn btn-primary" to={`/Paycard`}>
        Add Card
      </Link>
    </Container>
    </>
  );
}
